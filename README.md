# Taller Introducción a Base de Datos

Docente: Maximiliano Marcelo Andrada. <br>
Coordinadores: Dr. Cristian Martínez. Lic. Carlos Ismael Orozco

Acerca del curso: El mismo forma parte de una vinculación entre el Departamento de Informática ([DIUNSa](http://di.unsa.edu.ar)) y la Escuela de Arte y Oficio de la Provincia de Salta.

**Condiciones para la obtención de certificado:**
- Certificado de asistencia: un 70% de asistencia mínimo a las clases
- Constancia de aprobación: un 60% de asistencia mínimo a clases y aprobación de un trabajo integrador.

**Temario**

[[_TOC_]]

# Repaso general
Clase 1:
- [Presentación](https://gitlab.com/maxiandrada456/taller-introduccion-a-base-de-datos/-/blob/master/Presentaciones/13_-_Repaso_general.pdf) <br>
- [Video](https://youtu.be/A06wIGN9QB4) <br>

Clase 2:
- [Presentación](https://gitlab.com/maxiandrada456/taller-introduccion-a-base-de-datos/-/blob/master/Presentaciones/14_-_Repaso_general.pdf) <br>
- [Video](https://youtu.be/uFb5e27qKAw) <br>

Clase 3:
- [Tp Repaso general](https://gitlab.com/maxiandrada456/taller-introduccion-a-base-de-datos/-/blob/master/Trabajos%20Pr%C3%A1cticos/TP%20-%20Repaso%20general/TP_-_BD_Repaso_general.pdf) <br>
- [Video](https://youtu.be/NEq8AdSOGko) <br>

Clase 4:
- [BD Access - Videoclub](https://drive.google.com/file/d/1Awid30ks3uch4ERj62bCByBlq_RRJjMM/view?usp=sharing) <br>
- [Video](https://youtu.be/cJTOFPTmriQ) <br>

# Tema 1: Introducción
Clase 1:
- [Presentación]() <br>
- [Video]() <br>

Clase 2: 
- [Presentación]() <br>
- [Video]() <br>

Clase 3:
- [Presentación]() <br>
- [Video]() <br>

Clase 4:
- [Ejercicios]() <br>
- [Video]() <br>

# Tema 2: DER
Clase 5:
- [Presentación](https://drive.google.com/file/d/1F0yPIHktfdcHWj0naR_NWIQEwKRyZsr6/view?usp=sharing) <br>
- [Video](https://youtu.be/s-kXNrIChsg) <br>

Clase 6: 
- [Presentación](https://drive.google.com/file/d/1ohEYUTdDz5I5fSC4ldAtHq9iXo5xSNbv/view?usp=sharing) <br>
- [Video](https://youtu.be/4UsEfWY1-fU) <br>

Clase 7:
- [Presentación](https://drive.google.com/file/d/1Vzcl2hvS66UBB5c9SZ-_GSf_DTCbDM2g/view?usp=sharing) <br>
- [Trabajo Práctico](https://docs.google.com/document/d/1tepSPuJAo_EnSoyLSLLQeRQ3wvtq3DWmuSrDFAC8RDA/edit?usp=sharing) <br>
- [Video](https://youtu.be/BuUR6dzPruI) <br>

Clase 8:
- [Video](https://youtu.be/wgWpFKkqdOI) <br>

Clase 9: 
- [Video](https://youtu.be/iY087GRK6-g) <br>

Clase 10:
- [Coloquio](https://drive.google.com/drive/folders/1AZxJ2ipnrsMVHpxerOmVJWFPaPZmflzv?usp=sharing) <br>
- [Modelo de tablas](https://drive.google.com/file/d/1W2HXy-vRTQtiDMSIKAgPYvNmzxmOhpSV/view?usp=sharing) <br>
- [DER](https://drive.google.com/file/d/1c-3eUEkVT3BBkAz0PrHZm2-wyme5tJRZ/view?usp=sharing) <br>
- [Video](https://youtu.be/qyhL3rojbV4) <br>

Clase 11: 
- [Presentación - 07](https://drive.google.com/file/d/1Loz6LOWw7j5bRZH8gYbD17vCz0eva0AE/view?usp=sharing) <br>
- [Video](https://youtu.be/e7BGHNsnr9I) <br>

Clase 12:
- [Video](https://youtu.be/uKRFWjLP48w) <br>

Clase 13: 
- [Video](https://youtu.be/SpNRx78bpxU) <br>

Clase 14: 
- [DER TP2 - Narrativa b](https://drive.google.com/file/d/1kGn25sNq-FM9WISQ5VUcI_yIzWT3bGeo/view?usp=sharing) <br>
- [Diseño de tablas TP2 - Narrativa b](https://docs.google.com/spreadsheets/d/1KDS5DVGY-2nldDSwCm1Qx0Ve3iyeHGpwPMOqoPA-y4w/edit?usp=sharing) <br>
- [Video](https://youtu.be/x_zLsNCi77E) <br>

Clase 15: 
- [Video](https://youtu.be/A9t0T-y_fnc) <br>

# Tema 3: Introducción a SQL y Access
Clase 16: 
- [Presentación - 08](https://drive.google.com/file/d/1RxcnML9TqLbCKamGvgjfsptrf-ckLW1G/view?usp=sharing) <br>
- [Video](https://youtu.be/xyvAXaijsdo) <br>

Clase 17: 
- [Presentación - 09](https://drive.google.com/file/d/11GbpGEYeWC6AHhBJKm_iVJgO0rvuxXN_/view?usp=sharing) <br>
- [Video](https://youtu.be/9fqY8KqF3kk) <br>

Clase 18: 
- [Presentación - 10](https://drive.google.com/file/d/1AgjaHTwfrCFgO1M6YQQiZTFxKOH3aXVQ/view?usp=sharing) <br>
- [Video - Parte1](https://youtu.be/D6wc7gj38b8) <br>
- [Video - Parte2](https://youtu.be/GVrTbtKrMiM) <br>

Clase 19: 
- [Presentación - 11](https://drive.google.com/file/d/1eDPaAlgYBOzPNUe2X_uOLeP34JkZ2WEN/view?usp=sharing) <br>
- [DER - Facultades](https://drive.google.com/file/d/1yCqF5OkNHXMheytLfq2awxN8047H80OG/view?usp=sharing) <br>
- [Video](https://youtu.be/Z3yFLAz_Th4) <br>

Clase 20:
- [Presentación - 12](https://drive.google.com/file/d/1Rt3T6IlQOKnH6-imXytmNNsVyUb5xt2N/view?usp=sharing) <br>
- [Video](https://youtu.be/GHKDwkgq9qw) <br>

Clase 21:
- [Video](https://youtu.be/6U-TPwxOqIk) <br>

Clase 22:
- [BD - Cadetería](https://drive.google.com/file/d/1ByHvyvELNBl8jeS0lEpoze9laBIW_dw7/view?usp=sharing) <br>
- [Video](https://youtu.be/3VlqLgTQ9GE) <br>

Clase 23:
- [Tutorial Access 2010 y 2013](https://www.youtube.com/watch?v=HiNcD8ADxTA) <br>
- [Video](https://youtu.be/bxmQK6_mC2E) <br>

Clase 24:
- [Video - Parte1](https://youtu.be/UmO_lzVYxFk) <br>
- [Video - Parte2](https://youtu.be/GDr3EAPpzbQ) <br>

Clase 25 - Repaso DER y Normalización: 
- [Video](https://youtu.be/dhnjkQ3TV8M) <br>

Clase 26 - TP3: Ejercicio 2 (filtros):
- [Video - Parte1](https://youtu.be/EjCUMruDdko) <br>
- [Video - Parte2](https://drive.google.com/file/d/1i9umuBzuh5pi-GlYeN7XJetw_u1p5JgZ/view?usp=sharing) <br>

Clase 27 - Corrección Simulacro 2° Coloquio (Ejercicio 1):
- [BD Access - Videoclub](https://drive.google.com/file/d/10sWBPqwR5JHI_ehODqEOk-tZ-Wb9bUyT/view?usp=sharing) <br>
- [Video](https://youtu.be/mBvE32b8NEY) <br>

Clase 28 - Corrección Simulacro 2° Coloquio (Ejercicio 2) y TP3:
- [Video](https://youtu.be/n7K_IVOsDrY) <br>

Clase 29 - Access (Consultas, Formularios e Informes):
- [Video](https://youtu.be/MRXeD5rgtN8) <br>

Clase 30 - Access (Consultas, Formularios e Informes):
- [Video](https://youtu.be/DJ1-nWG7Cgg) <br>
- [BD Gestión de pedidos actualizada](https://drive.google.com/file/d/1o3qW-M0-wxcJlLGzKikUkYIbBBN3jTH2/view?usp=sharing) <br>

Clase 30 - Access (Consultas, Formularios e Informes):
- [Video](https://youtu.be/DJ1-nWG7Cgg) <br>
- [BD Gestión de pedidos actualizada](https://drive.google.com/file/d/1o3qW-M0-wxcJlLGzKikUkYIbBBN3jTH2/view?usp=sharing) <br>

Clase 31 - TP4 Access (Consultas, Formularios e Informes):
- [Video](https://youtu.be/Yf4G5N0Mh2c) <br>

Clase 32 - TP4 Access (Consultas, Formularios e Informes):
- [Video](https://youtu.be/gY4qgQYdtcU) <br>

Clase 33 - TP4 Access (Consultas, Formularios e Informes):
- [Video](https://youtu.be/T3oHgHJQGvo) <br>


